from datetime import datetime
from django.shortcuts import redirect, render
import pytz
import settings


def set_timezone(request):
    if request.method == 'POST':
        print request.POST
        request.session['django_timezone'] = request.POST['timezone']
        return redirect('/')
    else:
        return render(request, 'testtemplate.html',
                      {'timezones': pytz.common_timezones,
                       'TIME_ZONE': settings.TIME_ZONE,
                       'time': datetime.now()})