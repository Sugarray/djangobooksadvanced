from django.conf.urls import include, url
from django.contrib import admin
from reader.views import HomeView
from django.conf.urls.static import static
from booksnob import settings
from views import set_timezone

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', HomeView.as_view(), name='home'),
    url(r'^reader/', include('reader.urls')),
    url(r'^critic/', include('critic.urls')),
    url(r'timezone/$', set_timezone),
    url(r'^api-auth/', include('djangorestapitutorial.urls',
                               namespace='djangorestapitutorial')),
    url(r'^docs/', include('rest_framework_swagger.urls')),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + \
    static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
