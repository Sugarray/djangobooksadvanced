from django.http import HttpResponseRedirect
from django.views.generic import TemplateView, DetailView, ListView, View, \
    UpdateView
from django.shortcuts import render
from django.views.generic.edit import FormView
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, logout
from forms import AuthorForm
from reader.actions import get_books, get_authors
from reader.models import Author
import logging
from django.core.cache import cache, get_cache


cach = get_cache('default')
user_adds = logging.getLogger('reader_add_user')
user_updates = logging.getLogger('reader_update_user')


class HomeView(TemplateView):
    template_name = 'home.html'

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        context['authors'] = get_authors()
        context['books'] = get_books()
        context['cached_avatar'] = cach.get('user_avatar')
        return context


class AuthorView(FormView):
    template_name = 'new_author.html'
    success_url = '/reader/get_auth/all'
    form_class = AuthorForm

    def form_valid(self, form):
        self.request.session['user_avatar'] = str(form.cleaned_data['avatar'])
        user_adds.debug('user {0} {1} has been added'.format
                        (form.cleaned_data['first_name'],
                         form.cleaned_data['last_name']))

        self.object = form.save()
        return super(AuthorView, self).form_valid(form)


class GetAuthorView(DetailView, UpdateView):
    model = Author
    template_name = 'author_by_id.html'
    success_url = '/reader/get_auth/all'
    fields = '__all__'

    def form_valid(self, form):
        cach.set('user_avatar', str(form.cleaned_data['avatar']), 10)
        self.request.session['user_avatar'] = str(form.cleaned_data['avatar'])
        user_updates.debug('user with {0} {1} has been updated'.format
                           (form.cleaned_data['first_name'],
                            form.cleaned_data['last_name']))
        form.save()
        return super(GetAuthorView, self).form_valid(form)


class GetAuthorList(ListView):
    model = Author
    template_name = 'all_authors.html'

    def post(self, request):
        text = request.POST.get('search', 'senya')
        choose = request.POST.get('rad', '')
        if choose == 'Desc':
            ret = Author.objects.filter(first_name__icontains=text).order_by(
                '-id')
        else:
            ret = Author.objects.filter(first_name__icontains=text).order_by(
                'id')

        return render(request, self.template_name, {'author_list': ret})


class RegisterFormView(FormView):
    form_class = UserCreationForm
    success_url = '/critic/thanks'
    template_name = 'Login.html'

    def form_valid(self, form):
        form.save()
        return super(RegisterFormView, self).form_valid(form)


class AutheticationForm(FormView):
    form_class = AuthenticationForm
    success_url = '/reader/get_auth/all'
    template_name = 'Authentification.html'

    def form_valid(self, form):
        user = form.get_user()
        login(self.request, user)

        return super(AutheticationForm, self).form_valid(form)


class LogoutForm(View):
    def get(self, request):
        logout(request)
        return HttpResponseRedirect('/')
