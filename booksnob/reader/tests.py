from django.test import TestCase

from reader import actions
from reader import models


class Author(TestCase):
    def setUp(self):
        self.author = models.Author.objects.create(
            first_name='sometext',
            last_name='sometext',
            email='sometext'
        )
        self.book = models.Book.objects.create(
            author=self.author,
            name='sometextbook'
        )

    def test_get_authors(self):
        authors = actions.get_authors()
        self.assertEqual(authors, [self.author])

    def test_get_books(self):
        book = actions.get_books()
        self.assertEqual(book, [self.book])