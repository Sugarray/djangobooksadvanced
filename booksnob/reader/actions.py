from reader.models import Book, Author


def get_books():
    return [book for book in Book.objects.all()]


def get_authors():
    return [author_name for author_name in Author.objects.all()]