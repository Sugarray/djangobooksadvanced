from django.core.management.base import BaseCommand, CommandError
from optparse import make_option
from reader.models import Author


class Command(BaseCommand):
    help = 'New command.HALP!'

    def add_arguments(self, parser):
        parser.add_argument('poll_id', nargs='+', type=str)
        parser.add_argument('--delete',
                            action='store_true',
                            dest='delete',
                            default=False,
                            help='some info')

    def handle(self, *args, **options):
        if options['delete']:
            print 1
            author = Author.objects.filter(first_name=options['poll_id'][0])
            print author
            author.delete()
        else:
            raise CommandError('no such option')