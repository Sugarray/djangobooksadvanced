from django.contrib import admin
from reader.models import Author, Book, Reader, AuthorSubmitDate


class AuthorAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name', 'email', 'avatar')
    search_fields = ['first_name']
    list_filter = ('first_name',)


class Admin(admin.ModelAdmin):
    filter_horizontal = ('reader',)

admin.site.register(Author, AuthorAdmin)
admin.site.register(AuthorSubmitDate)
admin.site.register(Book)
admin.site.register(Reader, Admin)
