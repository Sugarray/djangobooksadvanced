# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('reader', '0006_auto_20150608_2139'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='author',
            name='avatar',
        ),
    ]
