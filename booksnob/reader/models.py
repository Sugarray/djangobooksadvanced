import datetime
from django.db import models
from reader.managers import AuthorManager
from django.db.models.signals import post_save
from django.dispatch import receiver


class Author(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=40)
    avatar = models.ImageField(upload_to="static/reader",
                               default="static/default_avatar.png")
    email = models.EmailField()
    filter_author = AuthorManager()
    objects = models.Manager()

    def __unicode__(self):
        return '{0} {1} {2}'.format(self.first_name, self.last_name, self.email)


class AuthorSubmitDate(models.Model):
    author = models.OneToOneField(Author)
    date = models.DateTimeField()


class Book(models.Model):
    author = models.ForeignKey(Author)
    name = models.CharField(max_length=100)

    def __unicode__(self):
        return str(self.name)


class Reader(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    age = models.IntegerField()
    reader = models.ManyToManyField(Book)

    def __unicode__(self):
        return str(
            '{0} {1},{2}'.format(self.first_name, self.last_name, self.age))


@receiver(post_save, sender=Author)
def set_date(instance, **kwargs):
    exists = Author.objects.filter(id=instance.id)
    print exists
    if not exists:
        dateitem = AuthorSubmitDate(author=instance, date=datetime.datetime.now())
        dateitem.save()