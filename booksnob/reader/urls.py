from django.conf.urls import url
from django.contrib.auth.decorators import login_required
from django.views.decorators.cache import cache_page
from views import AuthorView, GetAuthorView, GetAuthorList, RegisterFormView, \
    AutheticationForm, LogoutForm


urlpatterns = [

    url(r'^get_auth/$', login_required(AuthorView.as_view()),
        name='get_author'),
    url(r'^get_auth/(?P<pk>\d+)/$', login_required(GetAuthorView.as_view()),
        name='auth_by_id'),
    url(r'^get_auth/all/$',
        cache_page(60 * 60)(login_required(GetAuthorList.as_view())),
        name='get_author_all'),
    url(r'^register/$', RegisterFormView.as_view(), name='register'),
    url(r'^login/$', AutheticationForm.as_view(), name='login'),
    url(r'^logout/$', login_required(LogoutForm.as_view()), name='logout')
]