from django.conf.urls import url
from critic.views import ReviewView, ThanksView


urlpatterns = [
    url(r'^review/$', ReviewView.as_view(), name='review-book'),
    url(r'^thanks/$', ThanksView.as_view(), name='thanks'),
]
