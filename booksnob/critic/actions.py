from reader.models import Book

__author__ = 'Vladimir Volodavets'


def get_books():
    return [book for book in Book.objects.all()]