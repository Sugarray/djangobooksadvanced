from django.core.urlresolvers import reverse
from critic.forms import ReviewForm
from django.views.generic.edit import FormView
from django.views.generic import TemplateView


class ReviewView(FormView):
    template_name = 'bootstrap_review.html'
    success_url = '/critic/thanks'
    form_class = ReviewForm

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        print request.POST
        print form.errors
        return super(ReviewView, self).post(request, args, kwargs)

    def form_valid(self, form):
        print 1
        return super(ReviewView, self).form_valid(form)


class ThanksView(TemplateView):
    template_name = 'thanks.html'