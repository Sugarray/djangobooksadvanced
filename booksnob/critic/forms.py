from django import forms
from critic.actions import get_books

__author__ = 'Vladimir Volodavets'


class ReviewForm(forms.Form):
    books = [
        (book, book) for book in get_books()
    ]
    book_name = forms.ChoiceField(choices=books)
    text = forms.CharField(widget=forms.Textarea)

    def send_email(self):
        print('Book: {0}'.format(self.cleaned_data['book_name']))
        print('Message: {0}'.format(self.cleaned_data['text']))
