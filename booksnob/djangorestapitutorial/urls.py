from django.conf.urls import include, url
from django.contrib import admin
from views import ListAuthors, SingleAuthor
urlpatterns = [
    url(r'^testurl/$', ListAuthors.as_view()),
    url(r'^testurl/(?P<pk>\d+)/$', SingleAuthor.as_view())
]
