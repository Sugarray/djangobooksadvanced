from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions
from djangorestapitutorial.serializers import SampleClass
from reader.models import Author
from rest_framework import status
from rest_framework import mixins
from rest_framework import generics
from rest_framework.permissions import IsAuthenticated, AllowAny


class ListAuthors(mixins.ListModelMixin, mixins.CreateModelMixin,
                  generics.GenericAPIView):
    """
    All authors\n
    [GET] Returns list of authors\n
    [POST] Adds new author
    """
    permission_classes = (AllowAny,)
    serializer_class = SampleClass
    queryset = Author.objects.all()

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


class SingleAuthor(APIView):
    """
    Returns author by his id
    """
    permission_classes = (IsAuthenticated,)
    serializer_class = SampleClass
    query_set = Author.objects.all()

    def get(self, request, **kwargs):
        a = Author.objects.filter(id=kwargs['pk'])
        res = SampleClass(a, many=True)
        return Response(res.data)




