from rest_framework import serializers
from reader.models import Author


class SampleClass(serializers.ModelSerializer):
    class Meta:
        model = Author
        fields = ('first_name', 'last_name', 'avatar')

